#include "mygameengine.h"

GameObject cube, anotherCube;

float initialX = 2.0f;

Vector3 cubePosition(-5.0f, 0.0f, 0.0f);
Vector3 movement(0.5f, 0.0f, 0.0f);

void Initialize() {
	//std::cout << "This is called only once" << std::endl;
	//cube.SetPosition(initialX, 0.0f, 0.0f);
	cube.SetScale(0.5, 0.5, 0.5);
	cube.SetRotation(45, 0, 0, 1);
}

void Update() {
	//std::cout << "This is called every frame" << std::endl;

	cube.SetPosition(initialX, 0.0f, 0.0f);
	cube.DrawCube();

	anotherCube.SetPosition(cubePosition);


	anotherCube.SetScale(2.0f, 5.0f, 3.0f);
	anotherCube.DrawCube();
	initialX += 0.1f;

	//cubePosition = cubePosition.Add(movement);
	//cubePosition = cubePosition + movement;
	cubePosition += movement;
}

int main(int argc, char** argv) {
	PrepareEngine(&argc, argv);
}