#pragma once
#include<glm.h> 

class camera : public GameObject
{
public:
    camera();
    camera(int w, int h) : width(w), height(h) {};
    float fov = 45;
    float near = 0.1f;
    float far = 100.0f;
    int width = 1024;
    int height = 768;

private:
    
};