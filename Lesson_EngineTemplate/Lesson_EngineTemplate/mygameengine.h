#ifndef MYGAMEENGINE_H
#define MYGAMEENGINE_H

//include all the libraries and header files here
#include <iostream>
#include <glut.h>

#include "gameObject.h"

//functions that will handle the execution of our engine

//This function will only run once at the Initialization of the engine
void Initialize();
//This function will run every frame
void Update();
//This function will initialize the actual engine itself
void PrepareEngine(int *argc, char** argv);

#endif