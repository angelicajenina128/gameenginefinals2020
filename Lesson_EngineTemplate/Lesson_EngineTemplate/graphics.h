#ifndef GRAPHICS_H
#define GRAPHICS_H

#include "mygameengine.h"

void ChangeSize(int width, int height);
void RenderScene();

#endif
