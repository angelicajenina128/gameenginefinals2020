#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include "Vector3.h"

class GameObject {
private:
	//replace the three float values for position to use the Vector3 class
	//float xPosition, yPosition, zPosition;
	Vector3 position;
	Vector3 scale;
	//Idea: Create your own Color class that will accept r,g,b OR
	//Idea: Create your own Color class that will accept a vector3 that represents rgb or vector4 that represents rgba
	//Idea: Create a Vector4 class x, y, z, w: rotation, or color r,g,b,a
	float rotationAngle, xAxisRotation, yAxisRotation, zAxisRotation;
	//float xScale, yScale, zScale;
public:
	//Overload the constructor
	GameObject();
	//Getters and Setters
	void SetPosition(float x, float y, float z);
	//override the functions
	void SetPosition(Vector3 position);
	void SetRotation(float angle, float x, float y, float z);
	void SetScale(float x, float y, float z);

	//3D
	void DrawCube();
	void DrawSphere();
	void DrawCone();

	//2D
	void DrawSquare();
	void DrawCircle(float cx, float cy, float r, int num_segments);
	void DrawFillCircle();
	void DrawTriangle();
	
};

#endif