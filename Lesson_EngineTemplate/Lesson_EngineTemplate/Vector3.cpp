#include "Vector3.h"

#include <iostream>
#include <cmath>

Vector3::Vector3() {
	this->x = 0;
	this->y = 0;
	this->z = 0;
}

Vector3::Vector3(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

void Vector3::Print() {
	std::cout << "(" << x << ", " << y << ", " << z << ")" << std::endl;
}

void Vector3::SetValue(float x, float y, float z) {
	this->x = x;
	this->y = y;
	this->z = z;
}

Vector3 Vector3::GetValue() {
	Vector3 value(x, y, z);
	return value;
}

/*
float Vector3::getX() { return x; }
float Vector3::getY() { return y; }
float Vector3::getZ() { return z; }
*/

Vector3 Vector3::Add(const Vector3& other) {
	Vector3 sum;
	sum.x = this->x + other.x;
	sum.y = this->y + other.y;
	sum.z = this->z + other.z;
	return sum;
}

Vector3 Vector3::operator+(const Vector3& other) {
	Vector3 sum;
	sum.x = this->x + other.x;
	sum.y = this->y + other.y;
	sum.z = this->z + other.z;
	return sum;
}

Vector3& Vector3::operator+=(const Vector3& other) {
	this->x += other.x;
	this->y += other.y;
	this->z += other.z;
	return *this;
}

Vector3 Vector3::Subtract(const Vector3& other) {
	Vector3 difference;
	difference.x = this->x - other.x;
	difference.y = this->y - other.y;
	difference.z = this->z - other.z;
	return difference;
}

Vector3 Vector3::operator-(const Vector3& other) {
	Vector3 difference;
	difference.x = this->x - other.x;
	difference.y = this->y - other.y;
	difference.z = this->z - other.z;
	return difference;
}

Vector3& Vector3::operator-=(const Vector3& other) {
	this->x -= other.x;
	this->y -= other.y;
	this->z -= other.z;
	return *this;
}

Vector3 Vector3::Multiply(const Vector3& other) {
	Vector3 product;
	product.x = this->x * other.x;
	product.y = this->y * other.y;
	product.z = this->z * other.z;
	return product;
}

Vector3 Vector3::operator*(const Vector3& other) {
	Vector3 product;
	product.x = this->x * other.x;
	product.y = this->y * other.y;
	product.z = this->z * other.z;
	return product;
}

Vector3& Vector3::operator*=(const Vector3& other) {
	this->x *= other.x;
	this->y *= other.y;
	this->z *= other.z;
	return *this;
}

Vector3 Vector3::Divide(const Vector3& other) {
	Vector3 quotient;
	quotient.x = this->x / other.x;
	quotient.y = this->y / other.y;
	quotient.z = this->z / other.z;
	return quotient;
}

Vector3 Vector3::operator/(const Vector3& other) {
	Vector3 quotient;
	quotient.x = this->x / other.x;
	quotient.y = this->y / other.y;
	quotient.z = this->z / other.z;
	return quotient;
}

Vector3& Vector3::operator/=(const Vector3& other) {
	this->x /= other.x;
	this->y /= other.y;
	this->z /= other.z;
	return *this;
}

float Vector3::DotProduct(const Vector3& vector1, const Vector3& vector2) {
	//initalizes the float
	float dotproduct;
	//computes for the dot product
	dotproduct = (vector1.x * vector2.x) + (vector1.y * vector2.y) + (vector1.z * vector2.z);
	//returns the float
	return dotproduct;
}


Vector3 Vector3::CrossProduct(const Vector3& other) {
	//initalizes the vector
	Vector3 crossproduct;
	//computes for the cross product
	crossproduct.x = (this->y * other.z) - (this->z * other.y);
	crossproduct.y = (this->z * other.x) - (this->x * other.z);
	crossproduct.z = (this->x * other.y) - (this->y * other.x);
	//returns the vector
	return crossproduct;
}


float Vector3::Magnitude(const Vector3& vector) {
	//initalizes the float
	float magnitude;
	//computes for the magnitude
	magnitude = sqrt((pow(vector.x, 2) + pow(vector.y, 2) + pow(vector.z, 2)));
	//returns the float
	return magnitude;
}

Vector3 Vector3::Normalise(const Vector3& other) {
	//initalizes the vector
	Vector3 normal;

	//assigns values to the vector
	normal.x = this->x;
	normal.y = this->y;
	normal.z = this->z;

	//finds the magnitude of the vector and stores it
	float mag = Magnitude(normal);

	//normalises the vector
	normal.x = this->x / mag;
	normal.y = this->y / mag;
	normal.z = this->z / mag;

	//returns the vector
	return normal;
}