#ifndef VECTOR3_H
#define VECTOR3_H

class Vector3 {
public:
	float x, y, z;
	//Default Constructor
	Vector3();
	//Overloaded Constructor
	Vector3(float x, float y, float z);

	void Print();
	//Getter and Setter
	void SetValue(float x, float y, float z);
	Vector3 GetValue();
	//float getX();
	//float getY();
	//float getZ();

	//Mathematical Operation
	//Addition
	Vector3 Add(const Vector3& other);
	Vector3 operator+ (const Vector3& other);
	Vector3& operator+= (const Vector3& other);
	//TODO
	//Subtraction
	Vector3 Subtract(const Vector3& other);
	Vector3 operator- (const Vector3& other);
	Vector3& operator-= (const Vector3& other);
	//Scalar Multiplication
	Vector3 Multiply(const Vector3& other);
	Vector3 operator* (const Vector3& other);
	Vector3& operator*= (const Vector3& other);
	//Scalar Division
	Vector3 Divide(const Vector3& other);
	Vector3 operator/ (const Vector3& other);
	Vector3& operator/= (const Vector3& other);
	//Dot Product
	float DotProduct(const Vector3& vector1, const Vector3& vector2);
	//Cross Product
	Vector3 CrossProduct(const Vector3& other);
	//Get the magnitude of the vector
	float Magnitude(const Vector3& vector);
	//normalize the vector
	Vector3 Normalise(const Vector3& other);
};

#endif
