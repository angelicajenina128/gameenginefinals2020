#include "graphics.h"

void ChangeSize(int width, int height)
{
	// Prevent divide by 0
	if (height == 0)
		height = 1;
	float ratio = 1.0 * width / height;
	// Use the Projection Matrix
	glMatrixMode(GL_PROJECTION);
	// Reset Matrix
	glLoadIdentity();
	// Set the viewport to be the entire window
	glViewport(0, 0, width, height);
	// Set the correct perspective.
	gluPerspective(45, ratio, 1, 1000);
	// Set the camera's position
	gluLookAt(
		0.0, 0.0f, 15.0f, //camera position
		0.0f, 0.0, 0.0, //camera look direction
		0.0, 1.0f, 0.0f //direction of up vector
	);
	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void RenderScene() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	//The function that is called every frame
	Update();
	
	glutSwapBuffers();
}