#include "gameObject.h"
#include <math.h>
#include <glut.h>

//constructor
GameObject::GameObject() {
	SetPosition(0, 0, 0);
	SetRotation(0, 0, 0, 0);
	SetScale(1, 1, 1);
}

void GameObject::SetPosition(float x, float y, float z) {
	position.SetValue(x, y, z);
	//this->xPosition = x;
	//this->yPosition = y;
	//this->zPosition = z;
}

void GameObject::SetPosition(Vector3 position) {
	this->position = position;
}

void GameObject::SetRotation(float angle, float x, float y, float z) {
	this->rotationAngle = angle;
	this->xAxisRotation = x;
	this->yAxisRotation = y;
	this->zAxisRotation = z;
}

void GameObject::SetScale(float x, float y, float z) {
	scale.SetValue(x, y, z);
	//this->xScale = x;
	//this->yScale = y;
	//this->zScale = z;
}
//3D
void GameObject::DrawCube() {
	glPushMatrix();
	glTranslatef(this->position.getX(), this->position.getY(), this->position.getZ());
	//glColor3f(Color.r, Color.g, Color.b) or glColor3f(Color.x, Color.y, Color.z)
	glRotatef(this->rotationAngle, this->xAxisRotation, this->yAxisRotation, this->zAxisRotation);
	glScalef(this->scale.getX(), this->scale.getY(), this->scale.getZ());
	glutSolidCube(1.0f);
	glPopMatrix();
}

void GameObject::DrawSphere() {
	glPushMatrix();
	glTranslatef(this->position.getX(), this->position.getY(), this->position.getZ());
	//glColor3f(Color.r, Color.g, Color.b) or glColor3f(Color.x, Color.y, Color.z)
	glRotatef(this->rotationAngle, this->xAxisRotation, this->yAxisRotation, this->zAxisRotation);
	glScalef(this->scale.getX(), this->scale.getY(), this->scale.getZ());
	glutSolidSphere(2.0,2,2);
	glPopMatrix();
}

void GameObject::DrawCone() {
	glPushMatrix();
	glTranslatef(this->position.getX(), this->position.getY(), this->position.getZ());
	//glColor3f(Color.r, Color.g, Color.b) or glColor3f(Color.x, Color.y, Color.z)
	glRotatef(this->rotationAngle, this->xAxisRotation, this->yAxisRotation, this->zAxisRotation);
	glScalef(this->scale.getX(), this->scale.getY(), this->scale.getZ());
	glutSolidCone(2.0, 2, 2,1);
	glPopMatrix();
}

//2D
void GameObject::DrawSquare() {
	glPushMatrix();
	glTranslatef(this->position.getX(), this->position.getY(), this->position.getZ());
	//glColor3f(Color.r, Color.g, Color.b) or glColor3f(Color.x, Color.y, Color.z)
	glRotatef(this->rotationAngle, this->xAxisRotation, this->yAxisRotation, this->zAxisRotation);
	glScalef(this->scale.getX(), this->scale.getY(), this->scale.getZ());
	glClear(GL_COLOR_BUFFER_BIT);
	glColor3f(0.0, 1.0, 0.0);
	glBegin(GL_POLYGON);
	glVertex3f(1.0, 1.0, 1.0);
	glVertex3f(1.0, -1.0, 1.0);
	glVertex3f(-1.0, -1.0, 1.0);
	glVertex3f(-1.0, 1.0, 1.0);
	glEnd();	
	glPopMatrix();
}

void GameObject::DrawCircle(float cx, float cy, float r, int num_segments)
{
	glBegin(GL_LINE_LOOP);
	for (int ii = 0; ii < num_segments; ii++) {
		float theta = 2.0f * 3.1415926f * float(ii) / float(num_segments);//get the current angle 
		float x = r * cosf(theta);//calculate the x component 
		float y = r * sinf(theta);//calculate the y component 
		glVertex2f(x + cx, y + cy);//output vertex 
	}
}

void GameObject::DrawFillCircle()
{
	//static float angle;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(0, 0, -10);
	int i, x, y;
	double radius = 0.30;
	glColor3ub(255, 0, 0);
	double twicePi = 2.0 * 3.142;
	x = 0, y = 0;
	glBegin(GL_TRIANGLE_FAN); //BEGIN CIRCLE
	glVertex2f(x, y); // center of circle
	for (i = 0; i <= 20; i++) {
		glVertex2f(
			(x + (radius * cos(i * twicePi / 20))), (y + (radius * sin(i * twicePi / 20)))
		);
	}
}

void drawFillCircle() {
	//static float angle;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();
	glTranslatef(0, 0, -10);
	int i, x, y;
	double radius = 0.30;
	//glColor3ub(253, 184, 19);     
	glColor3ub(255, 0, 0);
	double twicePi = 2.0 * 3.142;
	x = 0, y = 0;
	glBegin(GL_TRIANGLE_FAN); //BEGIN CIRCLE
	glVertex2f(x, y); // center of circle
	for (i = 0; i <= 20; i++) {
		glVertex2f(
			(x + (radius * cos(i * twicePi / 20))), (y + (radius * sin(i * twicePi / 20)))
		);
	}
	glEnd();
}
	

